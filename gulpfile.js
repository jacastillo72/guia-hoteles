const {src, dest, task, watch, start, series, parallel} = require('gulp');
//"use strict";

//var gulp = require('gulp'),
const sass = require('gulp-sass');
const browserSync = require('browser-sync');
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');

task('sass', function(){
    src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('./css'));
});

task('sass:watch', function(){
    watch('./css/*.scss', ['sass']);
});

task('browser-sync', function(){
    var files = ['./*html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js'];
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

task('default', series('browser-sync', function(){
    start('sass:watch');
}));

task('clean',  function(){
    return del(['dist']);
});

task('copyfonts', function(){
    return src('./node-modules/open-iconic/font/fonts/*.{ttf, woff, eof, svg, ot, otf}*')
        .pipe(dest('./dist/fonts'));
});

task('imagemin', function(){
    return src('./images/*')
        .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(dest('dist/images'));
});

task('usemin', function(){
    return src('./*.html')
        .pipe(flatmap(function(stream, file){
            return stream.pipe(usemin({
                css: [rev()],
                html: [function() {return htmlmin({collapseWhitespace: true});}],
                js: [uglify(), rev()],
                inlinejs: [uglify()],
                inlinecss: [cleanCss(), 'concat']
            }));
        }))
        .pipe(dest('dist/'));
});

task('build', series('clean', 'copyfonts', 'imagemin', 'usemin'));